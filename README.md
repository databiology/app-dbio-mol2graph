# Mol2Graph app

CIAO application of the mol2graph code by Yuliia Orlova.

Input is a mol file, output are atom and adjacency files (automatically generated from the input)

CIAO full documentation https://learn.ciao.tools/
