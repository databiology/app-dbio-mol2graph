mol2graph converts MDL mol file format into graph representation of the molecule (adjacency matrix and list of atoms). 

The code takes MDL mol file as input and produces two output files that describe a molecule as a molecular graph: list of atoms and adjacency matrix.

to run the code:

g++ main.cpp -o mol2graph

./mol2graph 'species_1.mol' 'atoms_EL.txt' 'adjacency_EL.txt'


'species_1.mol' - input MDL mol file
'atoms_EL.txt' - output file which contains list of atoms
'adjacency_EL.txt'-  output file which contains adjacency matrix