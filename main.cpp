//
//  main.cpp
//  mol2graph
//
//  Created by Yuliia Orlova on 15/09/16.
//  Copyright © 2016 Yuliia Orlova. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

string input_file_name, output_file_name_1, output_file_name_2;

int N_atoms = 0;
int N_edges = 0;
string* atoms;
int** A;

void read_input_file(string input_file_name)
{
    
    ifstream inFile;
    inFile.open(input_file_name.c_str());
    
   // if (!inFile.is_open())
   //     throw logic_error( "File <" + input_file_name + " cannot open the file." );

    string data;
    getline( inFile, data );
    getline( inFile, data );
    getline( inFile, data );

    
    inFile >> N_atoms >> N_edges;
    getline(inFile, data);
    
    atoms = new string[N_atoms];
    
    A = new int*[N_atoms];
    for (int i = 0; i < N_atoms; i++)
        A[i] = new int[N_atoms];
    
    for (int i = 0; i< N_atoms; i++)
        for (int j = 0; j<N_atoms; j++)
            A[i][j] = 0;
    
    
    for (int i = 0; i < N_atoms; ++i){
        double dummy = 0;
        char c;
        inFile >> dummy >> dummy >> dummy >> c;
        
        atoms[i] = c;
        getline( inFile, data );
    }
    
    
    
    for (int i=0; i < N_edges; i++){
        int col = 0;
        int row = 0;
        int edge = 0;
        
        inFile >> col >> row >> edge;
        
        getline(inFile, data);
        
        // since C++ has zero based indexes, we decrement each of the columns to be constant with the input
        
        col = col-1;
        row = row-1;


        A[row][col] = edge;
        A[col][row] = edge;
        }

    inFile.close();
};

void write_output_file(string output_file_name_1, string output_file_name_2){

    ofstream output_file_1;
    ofstream output_file_2;
    
    output_file_1.open(output_file_name_1.c_str());
    output_file_2.open(output_file_name_2.c_str());
    if (output_file_1.is_open()==0){
        cout<<"error: could not create output file 1!"<<endl;
        exit(1);
    }

    if (output_file_2.is_open()==0){
        cout<<"error: could not create output file 2!"<<endl;
        exit(1);
    }
    for (int i = 0; i < N_atoms; i++){
        output_file_1<<atoms[i]<<endl;
        
        for (int j = 0; j< N_atoms; j++){
                output_file_2<<A[i][j]<<" ";
        }
        output_file_2<<endl;
    }
    output_file_1.close();
    output_file_2.close();
};


int main(int argc, const char * argv[]) {
    
    if(argc!=4){
        std::cout<<"error: wrong invocation!"<<endl;
        std::cout<<"try with:"<<endl;
        std::cout<<"motion <string input_file_name>"<<endl;
        return 0;
        
    }
     input_file_name=argv[1]; // file EL.mol
    output_file_name_1 = argv[2]; //file atoms_EL.txt - list of atom of the input molecule
    output_file_name_2 = argv[3]; // file adjacency_EL.txt - connectivity matrix of the input molecule 

   // example of output:
   // output_file_name_1="atoms_molecule_EL.txt";
   // output_file_name_2 = "adjacency_molecule_EL.txt";
    
    read_input_file(input_file_name );
    write_output_file(output_file_name_1, output_file_name_2);
    
    
    return 0;
}


