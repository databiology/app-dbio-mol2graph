#
#   Mol2XYZ Application Dockerfile
#

# app/dbio/base:4.2.3 is based on Ubuntu 18.04
FROM app/dbio/base:4.2.3

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN apt update && \
    apt install -y build-essential && \
    rm -rf /var/lib/apt/lists/*

# Adding source code and compiling it
COPY main.cpp /usr/local/bin/
RUN cd /usr/local/bin && \
    g++ main.cpp -o mol2graph && \
    rm main.cpp

# Entrypoint is defined as /usr/local/bin/main.sh in app/dbio/base:4.2.3
COPY main.sh  /usr/local/bin/main.sh
RUN  chmod +x /usr/local/bin/main.sh
