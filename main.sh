#!/bin/bash

# Mol2Graph main script

# Global variables definition
SCRATCH=/scratch
RESULTSDIR="$SCRATCH/results"
INPUTDIR="$SCRATCH/input"

# functions
falseexit() {
   echo "Application failed: $1"
   exit 1
}

IFS=$'\n'
# Perform task over all input files
for FILE in $(jq -r '.resources[].relativePath' "$SCRATCH/projection.json")
do
    INFILE="$INPUTDIR/$FILE"
    if [ -f "$INFILE" ]
    then
        echo "Converting $INFILE"
    else
        falseexit "$INFILE is missing"
    fi
    
    ATMFILE="$RESULTSDIR/atoms_$(basename $INFILE .mol).txt"
    ADJFILE="$RESULTSDIR/adjacency_$(basename $INFILE .mol).txt"
    CMD="mol2graph '$INFILE' '$ATMFILE' '$ADJFILE'"
    echo "Executing: $CMD"
    bash -c "$CMD" || falseexit "Error converting file"
done